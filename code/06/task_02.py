import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (x^6 + 8*x) / |x^2 - 7|^-1/5
Z2 = |(5 - x) / (6 - x)|
Z3 = 5*(|Z1 - 2*Z2|) / (0.2 * x)
'''


def main():
    try:
        x = float(input("Введіть значення x: "))
        d = abs(x ** 2 - 7) ** (-1 / 5)
        if d != 0:
            z1 = (x ** 6 + 8 * x) / d
            d = abs(6 - x)
            if d != 0:
                z2 = abs(5 - x) / d
                if (0.2 * x) != 0:
                    z3 = 5 * abs(((x ** 6 + 8 * x) / abs(x ** 2 - 7) ** (-1 / 5)) - 2 * (abs(5 - x) / abs(6 - x))) / (0.2 * x)
                    print(f"Z1 = {z1}")
                    print(f"Z2 = {z2}")
                    print(f"Z3 = {z3}")
                    sys.exit(0)
                else:
                    print("Некоректні дані. Знаменник не може дорівнювати нулю.")
                    sys.exit(1)
            else:
                print("Некоректні дані. Знаменник не може дорівнювати нулю.")
                sys.exit(1)
        else:
            print("Некоректні дані. Знаменник не може дорівнювати нулю.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числове значення для x.")
        sys.exit(1)


if __name__ == "__main__":
    main()
