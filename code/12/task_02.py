import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (cos(x - y))^2 / (cos(11/8*x + y))^2
Z2 = sin(x / (2 - y))
Z3 = Z1/y - x/Z2
'''


def main():
    try:
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))
        if (math.cos((11 / 8) * x + y)) ** 2 != 0:
            z1 = (math.cos(x - y)) ** 2 / (math.cos((11 / 8) * x + y)) ** 2
            if (2 - y) != 0:
                z2 = math.sin(x / (2 - y))
                if y != 0:
                    z3 = ((math.cos(x - y)) ** 2 - (math.cos((11 / 8) * x + y)) ** 2) / y - x / (math.sin(x / 2))
                    print(f"Z1 = {z1}")
                    print(f"Z2 = {z2}")
                    print(f"Z3 = {z3}")
                    sys.exit(0)
                else:
                    print("Некоректні дані. Знаменник не може дорівнювати нулю.")
                    sys.exit(1)
            else:
                print("Некоректні дані. Знаменник не може дорівнювати нулю.")
                sys.exit(1)
        else:
            print("Некоректні дані. Знаменник не може дорівнювати нулю.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)


if __name__ == "__main__":
    main()
